# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(557, 386)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(20, 70, 521, 271))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.fo_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.fo_label.setObjectName("fo_label")
        self.gridLayout.addWidget(self.fo_label, 2, 0, 1, 1)
        self.fo_textBrowser = QtWidgets.QTextBrowser(self.gridLayoutWidget)
        self.fo_textBrowser.setObjectName("fo_textBrowser")
        self.gridLayout.addWidget(self.fo_textBrowser, 2, 1, 1, 1)
        self.zh_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.zh_label.setObjectName("zh_label")
        self.gridLayout.addWidget(self.zh_label, 1, 0, 1, 1)
        self.zh_textBrowser = QtWidgets.QTextBrowser(self.gridLayoutWidget)
        self.zh_textBrowser.setObjectName("zh_textBrowser")
        self.gridLayout.addWidget(self.zh_textBrowser, 1, 1, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 1, 1, 1)
        self.gridLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(20, 20, 521, 41))
        self.gridLayoutWidget_2.setObjectName("gridLayoutWidget_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.f_lineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.f_lineEdit.setToolTipDuration(-1)
        self.f_lineEdit.setText("")
        self.f_lineEdit.setClearButtonEnabled(False)
        self.f_lineEdit.setObjectName("f_lineEdit")
        self.gridLayout_2.addWidget(self.f_lineEdit, 0, 1, 1, 1)
        self.f_label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.f_label.setObjectName("f_label")
        self.gridLayout_2.addWidget(self.f_label, 0, 0, 1, 1)
        self.error_label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.error_label.setEnabled(True)
        self.error_label.setObjectName("error_label")
        self.gridLayout_2.addWidget(self.error_label, 1, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 557, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Вычислятор"))
        self.fo_label.setText(_translate("MainWindow", "Коэффициенты\n"
"        Фурье:"))
        self.zh_label.setText(_translate("MainWindow", "Коэффициенты\n"
"   многочлена\n"
"   Жегалкина:"))
        self.pushButton.setText(_translate("MainWindow", "Вычислить"))
        self.f_lineEdit.setToolTip(_translate("MainWindow", "Длина должна соответствовать степени 2."))
        self.f_lineEdit.setPlaceholderText(_translate("MainWindow", "Введите вектор значений функции..."))
        self.f_label.setText(_translate("MainWindow", "f:"))
        self.error_label.setText(_translate("MainWindow", "Длина вектора значений должна соответствовать степени 2."))

