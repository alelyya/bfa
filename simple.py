from coefficients import fourier_coefficients as fc, zhegalkin_coefficients as zc, fourier_coefficients_precalc as fcp
from coefficients import zhegalkin_coefficients_triangle as zct


def main():
    f = input('Введите вектор значений функции: ')
    f = tuple(int(i) for i in f)
    try:
        #fcp(f)
        #zct(f)
        #fc(f)
        #print('Коэффициенты многочлена Жегалкина: ', zct(f))
        #print('Коэффициенты многочлена Жегалкина: ', zc(f))
        #print('Коэффициенты Фурье: ', fc(f))
    except AttributeError:
        print('Длинна вектора значений должна быть степенью двойки.')


while True:
    main()
    print('\n')
