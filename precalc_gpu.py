import pycuda.driver as drv
import numpy as np
import pycuda.tools
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
from pycuda.elementwise import ElementwiseKernel

n = 4096
num = 128

fourier_coefficients_gpu = ElementwiseKernel(
    "int maxiter, int a, int *output",
    """
    {
    int count;
    int number;
    for (int i = 0; i < maxiter; i++) {
        number = a & i;
        count = 0;
        while (number != 0) {
            number = number & (number - 1);
            count++;
            };
        count = count % 2;
        if (count == 1) {
            output[i] = -1;
        }
        else {
            output[i] = 1;
            }
        }
    }
    """)


for k in range(num):
    name = 'C://PythonStuff/BFA/precalc/%d.txt' % k
    step = int(n/num)
    with open(name, 'w') as f:
        for a in range(step*k, step*k+step):
            output = gpuarray.to_gpu(np.array([0 for i in range(n)]))
            fourier_coefficients_gpu(n, a, output)
            f.write(",".join(str(i) for i in output.get()))
            f.write('\n')
print(1)
