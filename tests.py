import unittest
from coefficients import power_of_two, fourier_coefficients, zhegalkin_coefficients, zhegalkin_coefficients_triangle


class TestSortedSequence(unittest.TestCase):

    def test_power_of_two(self):
        pot = power_of_two

        self.assertEqual(pot(len([])), False)
        self.assertEqual(pot(len([1])), False)
        self.assertEqual(pot(len([1, 2, 3, 4])), True)
        self.assertEqual(pot(len([1, 2, 3, 4, 5])), False)
        self.assertEqual(pot(len([i for i in range(8)])), True)
        self.assertEqual(pot(len([i for i in range(16)])), True)

    def test_fourier(self):
        fc = fourier_coefficients
        self.assertEqual(fc(f = (1,1,1,1,1,1,1,1)), (8,0,0,0,0,0,0,0))
        self.assertEqual(fc(f = (0,0,0,0,0,0,0,0)), (0,0,0,0,0,0,0,0))

    def test_zhegalkin(self):
        zc = zhegalkin_coefficients

        self.assertEqual(zc(f = (0,0,1,1,0,0,1,1)), (0,0,1,0,0,0,0,0))
        self.assertEqual(zc(f = (1,0,1,1,0,0,1,1)), (1,1,0,1,1,1,1,1))
        self.assertEqual(zc(f = (1,1,1,1,0,0,1,1)), (1,0,0,0,1,0,1,0))
        self.assertEqual(zc(f = (1,1,1,1,1,1,1,1)), (1,0,0,0,0,0,0,0))

    def test_zhegalkin_triangle(self):
        zc = zhegalkin_coefficients_triangle
        self.assertEqual(zc(f = (0,0,1,1,0,0,1,1)), (0,0,1,0,0,0,0,0))
        self.assertEqual(zc(f = (1,0,1,1,0,0,1,1)), (1,1,0,1,1,1,1,1))
        self.assertEqual(zc(f = (1,1,1,1,0,0,1,1)), (1,0,0,0,1,0,1,0))
        self.assertEqual(zc(f = (1,1,1,1,1,1,1,1)), (1,0,0,0,0,0,0,0))


if __name__ == '__main__':
    unittest.main()
