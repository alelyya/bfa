n = 32
num = 1
coefficients = [[0 for _ in range(n)] for _ in range(int(n/num))]


def dot_product(v: int, u: int):
    global n
    v = bin(v)[2:].zfill(n)
    u = bin(u)[2:].zfill(n)

    summ = 0
    for i in range(len(v)):
        if v[i] == '1' and u[i] == '1':
            summ += 1
    return summ % 2


for k in range(num):
    name = 'C://PythonStuff/BFA/precalc/%d.txt' % k
    with open(name, 'w') as f:
        for i in range(int((n*k)/num), int((n*k)/num+n/num)):
            for j in range(n):
                f.write(str((-1)**dot_product(i, j))+',')
            f.write('\n')

print(1)
