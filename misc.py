import math


def function_value(function):
    values = [[] for _ in range(len(bin(max(function))[2:]))]
    for value in function:
        for function_number in range(len(bin(max(function))[2:])):
            values[function_number].append(value >> function_number & 1)
    values.reverse()
    return values


def avalanche_test(function):
    values = function_value(function)
    var = [[0] for _ in range(5)]
    for i in range(int(math.log2(len(function)))):
        for j in range(len(function)):
            num = 0
            for k in range(int(math.log2(len(function)))):
                if values[k][j] != values[k][j ^ (2**i)]:
                    num=num+1
            var[num][0]=var[num][0]+1
    return var


def bit_independence_criterion(function):
    values = function_value(function)
    g = {1:[0,1],2:[0,2],3:[1,2],4:[0,3],5:[1,3],6:[2,3]}
    var = [[0] for _ in range(6)]
    for k in g:
        for i in range(int(math.log2(len(function)))):
            num = 0
            for j in range(len(function)):
                if values[g[k][0]][j] != values[g[k][0]][j ^ (2**i)] and values[g[k][1]][j] != values[g[k][1]][j ^ (2**i)]:
                    num=num+1
            var[k-1][0]=var[k-1][0]+num
    return var, g
