def power_of_two(num):
    return bin(num).count('1') == 1 and num != 1


def fourier_coefficients_precalc(f: tuple):
    coefficients = []
    m = len(f)
    for a in range(0, m, 32):
        name = 'C://PythonStuff/BFA/precalc/%d.txt' % int(a // 32)
        with open(name, 'r') as file:
            for line in file:
                summ = 0
                temp = [int(x) for x in line.split(',')]
                for i in range(m):
                    if f[i]:
                        summ += temp[i]
                coefficients.append(summ)
    return tuple(coefficients)


def fourier_coefficients(f: tuple):
    def dot_product(v: int, u: int):
        return bin(v & u).count('1') % 2

    m = len(f)
    coefficients = []
    cache = dict()

    for a in range(m):
        summ = 0
        for x in range(m):
            if f[x]:
                if (a, x) in cache.keys():
                    summ += cache[(a, x)]
                else:
                    if dot_product(a, x):
                        summ -= 1
                        cache[(a, x)] = -1
                        cache[(x, a)] = -1
                    else:
                        summ += 1
                        cache[(a, x)] = 1
                        cache[(x, a)] = 1
        coefficients.append(summ)
    return tuple(coefficients)


def zhegalkin_coefficients(f: tuple):
    cache = dict()

    def zeta(args):
        if args in cache.keys():
            return cache[args]

        if len(args) == 1:
            value = args[0]
        else:
            value = zeta(args[:-1]) ^ zeta(args[1:])
        cache[args] = value
        return value

    coefficients = tuple(zeta(f[0: i+1]) for i in range(len(f)))
    return coefficients


def zhegalkin_coefficients_triangle(f: tuple):
    n = len(f)
    coefficients = [[0 for _ in range(n)] for _ in range(n)]
    for i in range(n):
        coefficients[i][0] = f[i]
    for k in range(n-1, -1, -1):
        for i in range(k):
            coefficients[i][n-k] = coefficients[i][n-k-1] ^ coefficients[i+1][n-k-1]
    return tuple(coefficients[0])
