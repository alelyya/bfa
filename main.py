import sys
import qt
from coefficients import fourier_coefficients as fc, zhegalkin_coefficients_triangle as zct, power_of_two
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import pyqtSlot


class MainWindow(QMainWindow, qt.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.on_click)
        self.f_lineEdit.returnPressed.connect(self.on_click)
        self.error_label.hide()
        self.error_label.setStyleSheet('color: red')

    @pyqtSlot()
    def on_click(self):
        try:
            f = tuple(int(i) for i in self.f_lineEdit.text())
        except:
            self.clear_textBrowsers()
            self.error_label.setText('Введённый вектор значений содержит недопустимые символы.')
            self.error_label.show()
            return
        if not all(i in (1, 0) for i in f):
            self.clear_textBrowsers()
            self.error_label.setText('Введённый вектор значений содержит числа, отличные от 0 и 1.')
            self.error_label.show()
            return

        if not power_of_two(len(f)):
            self.clear_textBrowsers()
            self.error_label.setText('Длина вектора значений должна соответствовать степени 2.')
            self.error_label.show()
            return

        self.error_label.hide()
        zh_coeff = ", ".join(str(i) for i in zct(f))
        self.zh_textBrowser.setText(zh_coeff)
        fo_coeff = ", ".join(str(i) for i in fc(f))
        self.fo_textBrowser.setText(fo_coeff)

    def clear_textBrowsers(self):
        self.zh_textBrowser.setText(' ')
        self.fo_textBrowser.setText(' ')


def main():
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
