n = 12

def inverted(list_):
    return [1-i for i in list_]


def double(matrix):
    old_size = len(matrix)
    new_size = old_size * 2
    new = [[0 for _ in range(new_size)] for _ in range(new_size)]

    for i in range(new_size):
        if i < old_size:
            new[i] = matrix[i] * 2
        else:
            new[i] = matrix[i-old_size]
            new[i].extend(inverted(matrix[i-old_size]))
    return new


coefficients = [[0,0],[0,1]]
for i in range(n):
    coefficients = double(coefficients)

with open('precalc.txt', 'w') as f:
    for line in coefficients:
        str_line = ','.join(str(i) for i in line)
        f.write(str_line+'\n')
print(1)
